#import "cocos2d.h"
#import "Ship.h"
#import "Enemy.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "ContactListener.h"

@interface Game:CCLayer
{
    int points;
    
    CCSprite* bg;
    CCSprite* water;
    CCSprite* moon;
    
    CCSpriteBatchNode* shipBatch;
    CCSpriteBatchNode* ballBatch;
    CCSpriteBatchNode* enemiesBatch;
    
    Ship* ship1;
    Ship* ship2;
    
    CCArray* balls;
    CCArray* enemies;
    CCArray* explosions;
    
    b2World* world;
    ContactListener* contactListener;
	GLESDebugDraw* debugDraw;
}

@property (readonly, nonatomic) CCSpriteBatchNode* shipBatch;
@property (readonly, nonatomic) CCSpriteBatchNode* ballBatch;
@property (readonly, nonatomic) CCSpriteBatchNode* enemiesBatch;
@property (readonly, nonatomic) b2World* world;

-(void)initPhysics;
-(void)createEnvironment;
-(void)createEntities;
-(void)createBall:(CGPoint)v position:(CGPoint)p;
-(void)removeBall:(Ball*)ball;
-(void)createEnemy;
-(void)removeEnemy:(Enemy*)enemy;
-(void)raiseWater;
-(void)createExplosion:(CGPoint)location;
-(void)removeExplosion:(CCParticleExplosion*)explosion;

+(Game*) sharedInstance;

@end