#import "Ball.h"
#import "Ship.h"
#import "Game.h"
#import "Constants.h"

@implementation Ball

+(id)ballWithVelocity:(CGPoint)velocity position:(CGPoint)position
{
	return [[[self alloc] initWithVelocity:velocity position:(CGPoint)position] autorelease];
}

-(id)initWithVelocity:(CGPoint)velocity position:(CGPoint)position
{
    if ([self init])
    {
        view = [CCSprite spriteWithFile:@"ball.png"];
        view.position = position;
        
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position = b2Vec2(view.position.x/PTM_RATIO, view.position.y/PTM_RATIO);
        
        b2CircleShape circle;
        circle.m_radius = 0.42f;
        
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &circle;	
        fixtureDef.density = 1.0f;
        fixtureDef.friction = 0.3f;
        fixtureDef.restitution = 0.25f;
        
        [super createBodyInWorld:[[Game sharedInstance] world] bodyDef:&bodyDef fixtureDef:&fixtureDef batch:[Game sharedInstance].ballBatch];
        
        body->ApplyForce(b2Vec2(velocity.x, velocity.y), body->GetPosition());
        
        [[CCScheduler sharedScheduler] scheduleUpdateForTarget:self priority:0 paused:NO];
    }
    
    return self;
}

-(void)update:(ccTime)delta
{
    if (view.position.y < -PADDING/2)
    {
        [[Game sharedInstance] removeBall:self];
    }
}

-(void)destroy
{
    [[CCScheduler sharedScheduler] unscheduleUpdateForTarget:self];
}

@end