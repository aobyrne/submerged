#import "AboutScene.h"
#import "cocos2d.h"
#import "MenuScene.h"

@implementation AboutScene

-(id)init
{
    if (self = [super init])
    {
        CCMenuItem *menuItem1 = [CCMenuItemFont itemFromString:@"Back" target:self selector:@selector(onBack:)];
        
        CCMenu *menu = [CCMenu menuWithItems:menuItem1, nil];
        [menu alignItemsVertically];
        [self addChild:menu];
    }
    
    return self;
}

-(void)onBack:(id)sender
{
    CCTransitionFade *transition = [CCTransitionFade transitionWithDuration:0.2 scene:[MenuScene node]];
    [[CCDirector sharedDirector] replaceScene:transition];
}

+(id)scene
{
    CCScene *scene = [CCScene node];
    CCLayer *layer = [AboutScene node];
    [scene addChild:layer];
    return scene;
}

-(void)dealloc
{
    CCLOG(@"dealloc: %@", self);
    [super dealloc];
}

@end