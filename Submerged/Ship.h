#import "cocos2d.h"
#import "Box2D.h"
#import "BodyNode.h"

@class Ball;

@interface Ship:BodyNode <CCTargetedTouchDelegate>
{
    CCArray *balls;
}

-(id)initWithScale:(float)scale withRotation:(float)rotation withPosition:(CGPoint)position;
-(void)fireCanon;
-(void)destroy;

+(id)shipWithScale:(float)scale withRotation:(float)rotation withPosition:(CGPoint)position;

@end