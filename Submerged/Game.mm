#import "Game.h"
#import "Ball.h"
#import "Ship.h"
#import "ContactListener.h"
#import "Enemy.h"
#import "Constants.h"

@implementation Game

@synthesize world, shipBatch, ballBatch, enemiesBatch;

static Game* instance;
+(Game*) sharedInstance
{
	return instance;
}

-(id)init
{
	if (self = [super init])
    {
        instance = self;
        
        points = 100;
        
        [self initPhysics];
        [self createEnvironment];
        [self createEntities];
        [self scheduleUpdate];
        
        [self addChild:bg z:1];
        [self addChild:moon z:2];
        
        [self addChild:enemiesBatch z:3];
        [self addChild:ballBatch z:4];
        [self addChild:shipBatch z:5];
        
        [self addChild:water z:6];
	}
    
	return self;
}

-(void)update:(ccTime)delta
{
	int32 velocityIterations = 6;
	int32 positionIterations = 2;
	
	world->Step(delta, velocityIterations, positionIterations);
    
	for (b2Body* body = world->GetBodyList(); body != nil; body = body->GetNext())
	{
        BodyNode* bodyNode = (BodyNode*)body->GetUserData();
        
        if (bodyNode != NULL && bodyNode.view != nil)
        {
//            if ([bodyNode isKindOfClass:[Ship class]])
//            {
//                body->SetTransform(body->GetPosition(), -1 * CC_DEGREES_TO_RADIANS(bodyNode.view.rotation));
//            }
//            else
//            {
                bodyNode.view.position = CGPointMake(body->GetPosition().x*PTM_RATIO, body->GetPosition().y*PTM_RATIO);
//                bodyNode.view.rotation = -1 * CC_RADIANS_TO_DEGREES(body->GetAngle());
//            }
        }
	}
}

-(void)createEnvironment
{
    CGSize windowSize = [[CCDirector sharedDirector] winSize];
    
    bg = [CCSprite spriteWithFile:@"bg.png"];
    bg.position = ccp(bg.contentSize.width/2, bg.contentSize.height/2);
    bg.scale = 1.1;
    
    water = [CCSprite spriteWithFile:@"water.png"];
    water.position = ccp(water.contentSize.width/2, water.contentSize.height/2);
    water.scale = 1.1;
    water.opacity = 200.0;
    
    moon = [CCSprite spriteWithFile:@"moon.png"];
    moon.position = ccp(windowSize.width-moon.contentSize.width/2, windowSize.height-moon.contentSize.height/2);
    
    [bg stopAllActions];
    CCMoveTo *moveUp = [CCMoveTo actionWithDuration:1.5 position:ccp(bg.contentSize.width/2, bg.contentSize.height/2+5)];
    id ease1 = [CCEaseSineInOut actionWithAction:moveUp];
    CCMoveTo *moveDown = [CCMoveTo actionWithDuration:1.5 position:ccp(bg.contentSize.width/2, bg.contentSize.height/2-5)];
    id ease2 = [CCEaseSineInOut actionWithAction:moveDown];
    CCSequence *bgSequence = [CCSequence actions:ease1, ease2, nil];
    [bg runAction:[CCRepeatForever actionWithAction:bgSequence]];
    
    [water stopAllActions];
    CCMoveTo *moveLeft = [CCMoveTo actionWithDuration:0.6 position:ccp(water.contentSize.width/2+10,water.contentSize.height/2)];
    id ease3 = [CCEaseSineInOut actionWithAction:moveLeft];
    CCMoveTo *moveRight = [CCMoveTo actionWithDuration:0.6 position:ccp(water.contentSize.width/2-10,water.contentSize.height/2)];
    id ease4 = [CCEaseSineInOut actionWithAction:moveRight];
    CCSequence *waterSequence = [CCSequence actions:ease3, ease4, nil];
    [water runAction:[CCRepeatForever actionWithAction:waterSequence]];
}

-(void)createEntities
{
    CGSize windowSize = [[CCDirector sharedDirector] winSize];
    
    shipBatch = [CCSpriteBatchNode batchNodeWithFile:@"ship.png"];
    ballBatch = [CCSpriteBatchNode batchNodeWithFile:@"ball.png"];
    enemiesBatch = [CCSpriteBatchNode batchNodeWithFile:@"enemy.png"];
    
    ship1 = [Ship shipWithScale:0.65 withRotation:15.0 withPosition:ccp(windowSize.width*1/3-20, 140)];
    ship2 = [Ship shipWithScale:1 withRotation:-15.0 withPosition:ccp(windowSize.width*2/3+20, 140)];
    
    enemies = [[CCArray alloc] init];
    for ( int i = 0; i < MAX_ENEMIES; i++ )
    {
        [self createEnemy];
    }
    
    explosions = [[CCArray alloc] init];
    
    balls = [[CCArray alloc] init];
}

-(void)createBall:(CGPoint)v position:(CGPoint)p
{
    [balls addObject:[Ball ballWithVelocity:v position:p]];
}

-(void)removeBall:(Ball*)ball
{
    [ball destroy];
    [balls removeObject:ball];
}

-(void)createEnemy
{
    if (enemies.count < MAX_ENEMIES)
    {
        CGSize windowSize = [[CCDirector sharedDirector] winSize];
        CGPoint p = ccp(CCRANDOM_0_1() * windowSize.width, windowSize.height + PADDING*1/3 + CCRANDOM_0_1() * PADDING*2/3);
        
        Enemy* enemy = [Enemy enemyWithScale:.5 position:p];
        [enemies addObject:enemy];
    }
}

-(void)removeEnemy:(Enemy*)enemy
{
    [enemy destroy];
    [enemies removeObject:enemy];
}

-(void)createExplosion:(CGPoint)location
{
    CCParticleExplosion* explosion = [CCParticleExplosion node];
    
    explosion.autoRemoveOnFinish = YES;
    explosion.position = location;
    explosion.totalParticles = 100;
    explosion.life = 0;
    explosion.lifeVar = 0.5;
    explosion.startSize = 25;
    explosion.startSizeVar = 10;
    explosion.endSize = 0;
    explosion.endSizeVar = 0;
    explosion.speed = 100;
    explosion.speedVar = 100;
    ccColor4F varColor;
    varColor.r = 0.0f;
    varColor.g = 0.0f;
    varColor.b = 0.0f;
    varColor.a = 0.0f;
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    explosion.startColor = startColor;
    explosion.startColorVar = varColor;
    ccColor4F endColor;
    endColor.r = 1.0f;
    endColor.g = 1.0f;
    endColor.b = 1.0f;
    endColor.a = 0.0f;
    explosion.endColor = endColor;
    explosion.endColorVar = varColor;
    
    [explosions addObject:explosion];
    
    [self addChild:explosion z:8];
}

-(void)removeExplosion:(CCParticleExplosion*)explosion
{
    [explosions removeObject:explosion];
}

-(void)raiseWater
{
    CGSize windowSize = [[CCDirector sharedDirector] winSize];
    points = points - 2;
    [water setPosition:ccp(water.contentSize.width/2+10,100/points*windowSize.height)];
}

-(void)initPhysics
{
    CGSize screenSize = [CCDirector sharedDirector].winSize;
    
    
    b2Vec2 gravity = b2Vec2(0.0f, -9.8f);
	bool allowBodiesToSleep = true;
	world = new b2World(gravity, allowBodiesToSleep);
    
    
    contactListener = new ContactListener();
    world->SetContactListener(contactListener);
    
    
    b2BodyDef containerBodyDef;
	b2Body* containerBody = world->CreateBody(&containerBodyDef);
    
	float widthInMeters = screenSize.width/PTM_RATIO;
	float heightInMeters = screenSize.height/PTM_RATIO;
	b2Vec2 lowerLeftCorner = b2Vec2(0, -PADDING);
	b2Vec2 lowerRightCorner = b2Vec2(widthInMeters, -PADDING);
	b2Vec2 upperLeftCorner = b2Vec2(0, heightInMeters + PADDING);
	b2Vec2 upperRightCorner = b2Vec2(widthInMeters, heightInMeters + PADDING);
	
	b2PolygonShape screenBoxShape;
	int density = 0;
    
	screenBoxShape.SetAsEdge(upperLeftCorner, upperRightCorner);
	containerBody->CreateFixture(&screenBoxShape, density);
	screenBoxShape.SetAsEdge(lowerLeftCorner, lowerRightCorner);
	containerBody->CreateFixture(&screenBoxShape, density);
	screenBoxShape.SetAsEdge(upperLeftCorner, lowerLeftCorner);
	containerBody->CreateFixture(&screenBoxShape, density);
	screenBoxShape.SetAsEdge(upperRightCorner, lowerRightCorner);
	containerBody->CreateFixture(&screenBoxShape, density);
    
    
    debugDraw = new GLESDebugDraw(PTM_RATIO);
	world->SetDebugDraw(debugDraw);
	uint32 flags = 0;
	flags |= b2DebugDraw::e_shapeBit;
	flags |= b2DebugDraw::e_jointBit;
	//flags |= b2DebugDraw::e_aabbBit;
	//flags |= b2DebugDraw::e_pairBit;
	flags |= b2DebugDraw::e_centerOfMassBit;
	debugDraw->SetFlags(flags);
}

#ifdef DEBUG
-(void)draw
{
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	
	world->DrawDebugData();
	
	glEnable(GL_TEXTURE_2D);
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}
#endif

-(void)dealloc
{
    [self unscheduleUpdate];
    
    world->SetContactListener(NULL);
    
    [ship1 destroy];
    [ship2 destroy];
    
    for (Enemy* enemy in enemies)
    {
        [enemy destroy];
    }
    [enemies release];
    
    for (Ball* ball in balls)
    {
        [ball destroy];
    }
    [balls release];
    
    [explosions release];
    
    delete world;
	world = NULL;
    
    delete contactListener;
    contactListener = NULL;
    
	delete debugDraw;
    debugDraw = NULL;
    
	[super dealloc];
}

@end
