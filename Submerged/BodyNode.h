#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"

@interface BodyNode:CCNode 
{
	b2Body* body;
	CCSprite* view;
    CCSpriteBatchNode* batchNode;
}

@property (readonly, nonatomic) b2Body* body;
@property (readonly, nonatomic) CCSprite* view;

-(void) createBodyInWorld:(b2World*)world bodyDef:(b2BodyDef*)bodyDef fixtureDef:(b2FixtureDef*)fixtureDef batch:(CCSpriteBatchNode*)batch;

-(void) removeView;
-(void) removeBody;

@end
