#import "ContactListener.h"
#import "cocos2d.h"
#import "Ball.h"
#import "Enemy.h"
#import "Game.h"

void ContactListener::BeginContact(b2Contact* contact)
{
    b2Fixture* fixtureA = contact->GetFixtureA();
    b2Fixture* fixtureB = contact->GetFixtureB();
    
    b2Body* bodyA = fixtureA->GetBody();
    b2Body* bodyB = fixtureB->GetBody();
    
    BodyNode* bodyNodeA = (BodyNode*)bodyA->GetUserData(); 
    BodyNode* bodyNodeB = (BodyNode*)bodyB->GetUserData();
    
    if ([bodyNodeA isKindOfClass:[Ball class]] && [bodyNodeB isKindOfClass:[Enemy class]])
    {
        [[Game sharedInstance] removeBall:(Ball*)bodyNodeA];
        [[Game sharedInstance] removeEnemy:(Enemy*)bodyNodeB];
        [[Game sharedInstance] createExplosion:bodyNodeB.view.position];
    }
    else if ([bodyNodeA isKindOfClass:[Enemy class]] && [bodyNodeB isKindOfClass:[Ball class]])
    {
        [[Game sharedInstance] removeBall:(Ball*)bodyNodeB];
        [[Game sharedInstance] removeEnemy:(Enemy*)bodyNodeA];
        [[Game sharedInstance] createExplosion:bodyNodeA.view.position];
    }
    else if ([bodyNodeA isKindOfClass:[Enemy class]] && [bodyNodeB isKindOfClass:[Enemy class]])
    {
        [[Game sharedInstance] removeEnemy:(Enemy*)bodyNodeA];
        [[Game sharedInstance] removeEnemy:(Enemy*)bodyNodeB];
    }
}

void ContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
    
}

void ContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
    
}

void ContactListener::EndContact(b2Contact* contact)
{
    b2Fixture* fixtureA = contact->GetFixtureA();
    b2Fixture* fixtureB = contact->GetFixtureB();
    
    b2Body* bodyA = fixtureA->GetBody();
    b2Body* bodyB = fixtureB->GetBody();
    
    BodyNode* bodyNodeA = (BodyNode*)bodyA->GetUserData(); 
    BodyNode* bodyNodeB = (BodyNode*)bodyB->GetUserData();
    
    if ([bodyNodeA isKindOfClass:[Ball class]] && [bodyNodeB isKindOfClass:[Enemy class]])
    {
        [[Game sharedInstance] createEnemy];
    }
    else if ([bodyNodeA isKindOfClass:[Enemy class]] && [bodyNodeB isKindOfClass:[Ball class]])
    {
        [[Game sharedInstance] createEnemy];
    }
    else if ([bodyNodeA isKindOfClass:[Enemy class]] && [bodyNodeB isKindOfClass:[Enemy class]])
    {
        [[Game sharedInstance] createEnemy];
        [[Game sharedInstance] createEnemy];
    }
}