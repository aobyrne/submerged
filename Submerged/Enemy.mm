#import "Enemy.h"
#import "Game.h"
#import "Constants.h"

@implementation Enemy

+(id)enemyWithScale:(float)scale position:(CGPoint)position
{
	return [[[self alloc] initWithScale:scale position:(CGPoint)position] autorelease];
}

-(id)initWithScale:(float)scale position:(CGPoint)position
{
    if ([self init])
    {
        view = [CCSprite spriteWithFile:@"enemy.png"];
        view.position = position;
        view.opacity = 255;
        view.scale = 0.4 + scale * 0.8;
        
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position = b2Vec2(view.position.x/PTM_RATIO, view.position.y/PTM_RATIO);
        
        b2CircleShape circle;
        circle.m_radius = view.scale/2;
        
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &circle;	
        fixtureDef.density = 0.1f;
        fixtureDef.friction = 0.25f;
        fixtureDef.restitution = 0.6f;
        
        [super createBodyInWorld:[[Game sharedInstance] world] bodyDef:&bodyDef fixtureDef:&fixtureDef batch:[Game sharedInstance].enemiesBatch];
        
        [[CCScheduler sharedScheduler] scheduleUpdateForTarget:self priority:0 paused:NO];
    }
    
    return self;
}

-(void)update:(ccTime)delta
{
    if (view.position.y < -PADDING/2)
    {
        [[Game sharedInstance] removeEnemy:self];
        [[Game sharedInstance] createEnemy];
//        [[Game sharedInstance] raiseWater];
    }
}

-(void)destroy
{
    [[CCScheduler sharedScheduler] unscheduleUpdateForTarget:self];
}

@end