#import "BodyNode.h"
#import "Game.h"

@implementation BodyNode

@synthesize body;
@synthesize view;

-(void)createBodyInWorld:(b2World*)world bodyDef:(b2BodyDef*)bodyDef fixtureDef:(b2FixtureDef*)fixtureDef batch:(CCSpriteBatchNode*)batch
{
    batchNode = batch;
    
	[batchNode addChild:view];
	
	body = world->CreateBody(bodyDef);
	body->SetUserData(self);
	body->CreateFixture(fixtureDef);
}

-(void)removeView
{
	if (view != nil)
	{
//        [batchNode.children removeObject:view];
        [view removeFromParentAndCleanup:YES];
        view = nil;
    }
}

-(void)removeBody
{
    if (body != NULL)
	{
        body->GetWorld()->DestroyBody(body);
        body = NULL;
    }
}

-(void)dealloc
{
	[self removeView];
	[self removeBody];
	
	[super dealloc];
}

@end
