#import "cocos2d.h"

@interface MenuScene:CCLayer
{
    
}

-(void)transitionToScene:(CCScene*)s time:(ccTime)t;
- (void)onPlay:(id)sender;
- (void)onSettings:(id)sender;
- (void)onAbout:(id)sender;
+(id)scene;

@end
