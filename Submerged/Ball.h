#import "cocos2d.h"
#import "Box2D.h"
#import "BodyNode.h"

@class Ship;

@interface Ball:BodyNode
{
    
}

-(id)initWithVelocity:(CGPoint)velocity position:(CGPoint)position;
-(void)update:(ccTime)delta;
-(void)destroy;

+(id)ballWithVelocity:(CGPoint)velocity position:(CGPoint)position;

@end