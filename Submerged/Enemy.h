#import "cocos2d.h"
#import "Box2D.h"
#import "BodyNode.h"

@interface Enemy:BodyNode
{
    
}

-(id)initWithScale:(float)scale position:(CGPoint)position;
-(void)destroy;

+(id)enemyWithScale:(float)scale position:(CGPoint)position;

@end