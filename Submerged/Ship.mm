#import "Ship.h"
#import "Ball.h"
#import "Game.h"
#import "Constants.h"

@implementation Ship

+(id)shipWithScale:(float)scale withRotation:(float)rotation withPosition:(CGPoint)position
{
	return [[[self alloc] initWithScale:scale withRotation:rotation withPosition:(CGPoint)position] autorelease];
}

-(id)initWithScale:(float)scale withRotation:(float)rotation withPosition:(CGPoint)position
{
    if ([self init])
    {
        view = [CCSprite spriteWithFile:@"ship.png"];
        view.scale = scale;
        view.position = position;
        
        
//        b2BodyDef bodyDef;
//        bodyDef.type = b2_staticBody;
//        bodyDef.position = b2Vec2(view.position.x/PTM_RATIO, view.position.y/PTM_RATIO);
//        
//        b2PolygonShape square;
//        square.SetAsBox(1.0f, 1.0f);
//        
//        b2FixtureDef fixtureDef;
//        fixtureDef.shape = &square;	
//        fixtureDef.density = 0.5f;
//        fixtureDef.friction = 0.2f;
//        fixtureDef.restitution = 0.25f;
//        
//        [super createBodyInWorld:[[Game sharedInstance] world] bodyDef:&bodyDef fixtureDef:&fixtureDef batch:[Game sharedInstance].shipBatch];
        [[Game sharedInstance].shipBatch addChild:view];
        
        [view stopAllActions];
        CCRotateTo *rotateLeft = [CCRotateTo actionWithDuration:2.5 angle:rotation];
        id ease1 = [CCEaseSineInOut actionWithAction:rotateLeft];
        CCRotateTo *rotateRight = [CCRotateTo actionWithDuration:2.5 angle:-rotation];
        id ease2 = [CCEaseSineInOut actionWithAction:rotateRight];
        CCSequence *sequence = [CCSequence actions:ease1, ease2, nil];
        [view runAction:[CCRepeatForever actionWithAction:sequence]];
        
        
        [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
    }
    
    return self;
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint touchLocation = [view.parent convertTouchToNodeSpace:touch];
    if (CGRectContainsPoint([view boundingBox], touchLocation))
    {
        [self fireCanon];
    }
    return YES;
}

-(void)fireCanon
{
    float rot = CC_DEGREES_TO_RADIANS(view.rotation);
    float sinrot = sinf(rot);
    float cosrot = cosf(rot);
    
    CGPoint p;
    p.x = view.position.x + sinrot * 20;
    p.y = view.position.y + cosrot * 20;
    
    CGPoint v;
    v.x = sinrot * 450;
    v.y = cosrot * 450;
    
    [[Game sharedInstance] createBall:v position:p];
}

-(void)destroy
{
    [[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
    
    for (Ball* ball in balls)
    {
        [ball destroy];
    }
    [balls release];
}

@end