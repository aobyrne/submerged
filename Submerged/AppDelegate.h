#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate:NSObject <UIApplicationDelegate>
{
	UIWindow *window;
}

@property (nonatomic, retain) UIWindow *window;

@end
