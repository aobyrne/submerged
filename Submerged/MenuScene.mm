#import "MenuScene.h"
#import "PlayScene.h"
#import "SettingsScene.h"
#import "AboutScene.h"
#import "cocos2d.h"

@implementation MenuScene

- (id)init
{
    if (self = [super init])
    {
        CCMenuItem *menuItem1 = [CCMenuItemFont itemFromString:@"Play" target:self selector:@selector(onPlay:)];
        CCMenuItem *menuItem2 = [CCMenuItemFont itemFromString:@"Settings" target:self selector:@selector(onSettings:)];
        CCMenuItem *menuItem3 = [CCMenuItemFont itemFromString:@"About" target:self selector:@selector(onAbout:)];
        
        CCMenu *menu = [CCMenu menuWithItems:menuItem1, menuItem2, menuItem3, nil];
        [menu alignItemsVertically];
        [self addChild:menu];
    }
    
    return self;
}

+(id)scene
{
    CCScene *scene = [CCScene node];
    id node = [MenuScene node];
    [scene addChild:node];
    return scene;
}

- (void)onPlay:(id)sender
{
    [self transitionToScene:[PlayScene scene] time:0.2];
}

- (void)onSettings:(id)sender
{
    [self transitionToScene:[SettingsScene scene] time:0.2];
}

- (void)onAbout:(id)sender
{
    [self transitionToScene:[AboutScene scene] time:0.2];
}

-(void)transitionToScene:(CCScene*)s time:(ccTime)t
{
    CCTransitionFade *transition = [CCTransitionFade transitionWithDuration:t scene:s];
    [[CCDirector sharedDirector] replaceScene:transition];
}

-(void)onEnter
{
    [super onEnter];
}

-(void)onExit
{
    [super onExit];
}

-(void)dealloc
{
    CCLOG(@"dealloc: %@", self);
    [super dealloc];
}

@end
