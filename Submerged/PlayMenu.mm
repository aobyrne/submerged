#import "PlayMenu.h"
#import "MenuScene.h"
#import "cocos2d.h"

@implementation PlayMenu

-(id)init
{
    if (self = [super init])
    {
        CCMenuItem *menuItem1 = [CCMenuItemFont itemFromString:@"Back" target:self selector:@selector(onBack:)];
        CCMenu *menu = [CCMenu menuWithItems:menuItem1, nil];
        [menu alignItemsVertically];
        menu.position = ccp(menu.position.x,40);
        [self addChild:menu];
    }
    
    return self;
}

-(void)onBack:(id)sender
{
    CCTransitionFade *transition = [CCTransitionFade transitionWithDuration:0.2 scene:[MenuScene node]];
    [[CCDirector sharedDirector] replaceScene:transition];
}

@end
