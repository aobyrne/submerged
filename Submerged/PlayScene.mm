#import "PlayScene.h"
#import "Game.h"
#import "PlayMenu.h"

@implementation PlayScene

-(id)init
{
    if (self = [super init])
    {
        [self addChild:[Game node]];
        [self addChild:[PlayMenu node]];
    }
    
    return self;
}

+(id)scene
{
    CCScene *scene = [CCScene node];
    CCLayer *layer = [PlayScene node];
    [scene addChild:layer];
    return scene;
}

-(void)dealloc
{
    CCLOG(@"dealloc: %@", self);
    [super dealloc];
}

@end
