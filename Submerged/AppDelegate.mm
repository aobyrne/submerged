#import "cocos2d.h"
#import "AppDelegate.h"
#import "GameConfig.h"
#import "MenuScene.h"

@implementation AppDelegate

@synthesize window;

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
	CC_DIRECTOR_INIT();
    
    CCDirector *director = [CCDirector sharedDirector];
    
    EAGLView *view = [director openGLView];
	[view setMultipleTouchEnabled:YES];
    
    [CCTexture2D setDefaultAlphaPixelFormat:kTexture2DPixelFormat_RGBA8888];
    
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
	[director setDisplayFPS:YES];
	[director runWithScene:[MenuScene scene]];
}

-(void)applicationWillResignActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] pause];
}

-(void)applicationDidBecomeActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] resume];
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
}

-(void)applicationDidEnterBackground:(UIApplication*)application
{
	[[CCDirector sharedDirector] stopAnimation];
}

-(void)applicationWillEnterForeground:(UIApplication*)application
{
	[[CCDirector sharedDirector] startAnimation];
}

-(void)applicationWillTerminate:(UIApplication *)application
{
	CC_DIRECTOR_END();
}

-(void)applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

-(void)dealloc
{
	[[CCDirector sharedDirector] release];
	[window release];
	[super dealloc];
}

@end